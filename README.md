# gtk-sharp-build

A Debian based OCI image for building the GTK# version of Borepin

For available builds have a look at the [Container Registry](https://gitlab.com/fabinfra/gtk-sharp-build/container_registry)