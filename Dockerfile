FROM mono:slim

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y mono-complete gtk-sharp2 nuget capnproto git ruby ruby-dev build-essential nodejs npm
RUN gem install bundler
RUN npm install --location=global appcenter-cli
